<?php

class Building {

    protected $name;
    private $floors;
    public $address;


    public function __construct($name,$floors,$address){
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;

    }

    public function getName(){
      return $this->name;
    }
    public function setName($name){
      $this->name = $name;
    }

    public function getFloors(){
      return $this->floors;
    }
    public function setFloors($floors){
      $this->floors = $floors;
    }

    public function getAddress(){
      return $this->address;
    }
    public function setAddress($address){
      $this->address = $address;
    }

}

class Condominium extends Building{

}

$bldg = new Building('Caswyn Building',8,'Timog Avenue, Quezon City, Philippines');

$condo = new Condominium('Enzo Condo',5,'Buendia Avenue, Makati City, Phillipines');