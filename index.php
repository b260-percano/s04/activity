<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>

<head>
  <meta charset='utf-8'>
  <meta http-equiv='X-UA-Compatible' content='IE=edge'>
  <title>S04 Activity</title>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
  <script src='main.js'></script>
</head>

<body>
  <h1>Building</h1>
  <p>The name of the building is <?= $bldg->getName() ?></p>
  <p>The <?= $bldg->getName() ?> has <?= $bldg->getFloors() ?> floors.</p>
  <p>The <?= $bldg->getName() ?> is located at <?= $bldg->getAddress() ?></p>
  <p>The name of the building has been changed from <?= $bldg->getName() ?> <?php $bldg->setName('Caswynn Complex') ?> to <?= $bldg->getName() ?></p>
  <br>
  <h1>Condominium</h1>
  <p>The name of the building is <?= $condo->getName() ?></p>
  <p>The <?= $condo->getName() ?> has <?= $condo->getFloors() ?> floors.</p>
  <p>The <?= $condo->getName() ?> is located at <?= $condo->getAddress() ?></p>
  <p>The name of the building has been changed from <?= $condo->getName() ?> <?php $condo->setName('Enzo Tower') ?> to <?= $condo->getName() ?></p>

</body>

</html>